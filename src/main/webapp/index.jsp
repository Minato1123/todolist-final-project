<%-- 
    Document   : index
    Created on : 2022年11月29日, 上午10:49:25
    Author     : carrie
--%>

<%@page import="java.util.List"%>
<%@page import="com.mycompany.todolist.TodoItem"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style><%@include file="/WEB-INF/style.css"%></style>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@300&family=Yellowtail&display=swap" rel="stylesheet">
        <script src="https://code.iconify.design/iconify-icon/1.0.1/iconify-icon.min.js"></script>
        <script src="//unpkg.com/alpinejs" defer></script>
        <title>Todo-List</title>
    </head>
    <body>
        <div class="container">
            <div class="title">
                Todo-List
            </div>
            <div class="todo-list" x-data="{ editingId: -1 }">
                <%
                    List<TodoItem> todoItems = (List<TodoItem>) session.getAttribute("todoItems");
                    for (TodoItem item: todoItems) {
                %>
                <div x-data="{ id: <%= item.getId() %>, itemName: '<%= item.getItemName() %>', isDone: <%= item.getIsDone() %>, get isEdit() { return this.id === editingId }, editingName: '' }" class="todo-item">
                    <template x-if="isEdit">
                        <form method="post" action="updateItemName">
                            <input class="hidden" name="id" x-bind:value="id" />
                            <input class="edit-input" name="itemName" type="text" x-model="editingName" />
                            <button x-bind:disabled="!editingName.trim()" type="submit"><iconify-icon class="item-btn" icon="material-symbols:check"></iconify-icon></button>
                            <button @click.prevent="editingId = -1"><iconify-icon class="item-btn" icon="ic:round-close"></iconify-icon></button>
                        </form>
                    </template>
                    <template x-if="!isEdit">
                        <div class="todo-item">
                            <form method="post" action="updateItemStatus">
                                <input class="hidden" name="isDone" x-bind:value="isDone" />
                                <input class="hidden" name="id" x-bind:value="id" />
                                <button x-on:click="isDone = !isDone">
                                    <iconify-icon x-show="!isDone" class="item-btn" icon="carbon:checkbox"></iconify-icon>
                                    <iconify-icon x-show="isDone" class="item-btn" icon="carbon:checkbox-checked"></iconify-icon>
                                </button>
                            </form>
                            <div x-text="itemName" class="item"></div>
                            <div class="item-buttons">
                                <button x-on:click="editingId = id; editingName = itemName"><iconify-icon class="item-btn" icon="material-symbols:edit"></iconify-icon></button>
                                <form method="post" action="deleteItemServlet">
                                    <input class="hidden" name="id" x-bind:value="id" />
                                    <button><iconify-icon class="item-btn" icon="ic:outline-delete"></iconify-icon></button>
                                </form>
                            </div>
                        </div>
                    </template>
                </div>
                <%
                    }
                %>
            </div>
            <div x-data="{ itemName: '' }" class="todo-input">
                <form method="post" action="addTodoItemServlet">
                    <input name="itemName" type="text" x-model="itemName" />
                    <button x-bind:disabled="!itemName.trim()" type="submit">
                        <iconify-icon class="item-btn" icon="material-symbols:add"></iconify-icon>
                    </button>
                </form>
            </div>
        </div>
        
    </body>
</html>
